# -*- coding: utf-8 -*-
"""
Created on Fri Apr 29 15:27:07 2016
Data Enrichment
@author: Administrator
"""

import MySQLdb
import json
import pandas as pd
import sqlalchemy
from pandas.io import sql
from sqlalchemy import create_engine

def establish_DBconn(schema): 
    """
    Establish connection with the database
    """
    with open("config.json", 'r') as f:
        config = json.load(f)
    
    cnx = { 'host': config["host"],'user':config["user"],'password': config["password"],'db': schema}
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor() 
    return conn,cursor
    

def get_WellPids(cursor,schema,table_name):
    """
    Gets the well ids from the table data is being retrieved. 
    """
    #start=time.time()
    print("Reading well IDs")
    query = "SELECT distinct(API_prod) FROM "+schema+"."+table_name+";"
    cursor.execute(query)
    row   = cursor.fetchall()
    unique_pids=[v[0] for v in row]
    
    return sorted(unique_pids)
    
    
def create_table(conn,cursor,schema,table_name):
    """
    Create a new table to push the results 
    """    
    
    
    query = "CREATE TABLE IF NOT EXISTS `"+schema+"`.`"+table_name+"` (\
  `API_prod` BIGINT(20) NOT NULL COMMENT '',`norm_date` INT NULL COMMENT '',`report_date_mo` DATE NOT NULL COMMENT '',\
  `proddays` INT NULL COMMENT '',`oil_pd` DOUBLE NULL COMMENT '',\
  `oil_pd_fore_mean` DOUBLE NULL COMMENT '',`oil_pd_fore_std` DOUBLE NULL COMMENT '',\
  `oil_pd_fore_p10` DOUBLE NULL COMMENT '',`oil_pd_fore_p90` DOUBLE NULL COMMENT '',\
  `oil_mo` DOUBLE NULL COMMENT '', `oil_mo_fore_mean` DOUBLE NULL COMMENT '',\
  `cum_oil_mo` DOUBLE NULL COMMENT '', `cum_oil_mo_fore_mean` DOUBLE NULL COMMENT '',\
  `wtr_pd` DOUBLE NULL COMMENT '',`wtr_pd_fore_mean` DOUBLE NULL COMMENT '',\
  `wtr_pd_fore_std` DOUBLE NULL COMMENT '',`wtr_pd_fore_p10` DOUBLE NULL COMMENT '',\
  `wtr_pd_fore_p90` DOUBLE NULL COMMENT '',`wtr_mo` DOUBLE NULL COMMENT '',\
  `wtr_mo_fore_mean` DOUBLE NULL COMMENT '',`cum_wtr_mo` DOUBLE NULL COMMENT '',\
  `cum_wtr_mo_fore_mean` DOUBLE NULL COMMENT '',`wor` DOUBLE NULL COMMENT '',\
  `wor_fore_mean` DOUBLE NULL COMMENT '',`gross_mo` DOUBLE NULL COMMENT '',\
  `gross_mo_fore_mean` DOUBLE NULL COMMENT '',`fo_mo` DOUBLE NULL COMMENT '',\
  `fo_mo_fore_mean` DOUBLE NULL COMMENT '',`fw_mo` DOUBLE NULL COMMENT '',\
  `fw_mo_fore_mean` DOUBLE NULL COMMENT '');" 
    
    cursor.execute(query)
    
def write_DB(conn,cursor,schema,table_name,results):
    """
    Write the results to the database
    """
    
    query = "INSERT INTO `"+schema+"`.`"+table_name+"` (`API_prod`,`norm_date`,\
    `report_date_mo`,`proddays`,`oil_pd`,`oil_pd_fore_mean`,`oil_pd_fore_std`,`oil_pd_fore_p10`,\
    `oil_pd_fore_p90`,`oil_mo`,`oil_mo_fore_mean`,`cum_oil_mo`,`cum_oil_mo_fore_mean`,\
    `wtr_pd`,`wtr_pd_fore_mean`,`wtr_pd_fore_std`,`wtr_pd_fore_p10`,`wtr_pd_fore_p90`,`wtr_mo`,\
    `wtr_mo_fore_mean`,`cum_wtr_mo`,`cum_wtr_mo_fore_mean`,`wor`,`wor_fore_mean`,`gross_mo`,\
    `gross_mo_fore_mean`,`fo_mo`,`fo_mo_fore_mean`,`fw_mo`,`fw_mo_fore_mean`)\
    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,\
    %s,%s,%s,%s,%s,%s,%s,\
    %s,%s,%s,%s,%s,%s,%s,\
    %s,%s,%s,%s,%s,%s,%s,%s);"
 
    for API in results.keys() :
        a = results[API]
        for i in range(len(a["Welldates"])):
            output = (API,i+1,a["Welldates"][i],a["proddays"][i],a["oil_pd"][i],a["oil_pd_fore_mean"][i],a["oil_pd_fore_std"][i],a["oil_pd_fore_p10"][i],
                  a["oil_pd_fore_p90"][i],a["oil_mo"][i],a["oil_mo_fore_mean"][i],a["cum_oil_mo"][i],a["cum_oil_mo_fore_mean"][i],
                  a["wtr_pd"][i],a["wtr_pd_fore_mean"][i],
                  a["wtr_pd_fore_std"][i],a["wtr_pd_fore_p10"][i],a["wtr_pd_fore_p90"][i],a["wtr_mo"][i],a["wtr_mo_fore_mean"][i],a["cum_wtr_mo"][i],
                  a["cum_wtr_mo_fore_mean"][i],a["wor"][i],a["wor_fore_mean"][i],a["gross_mo"][i],a["gross_mo_fore_mean"][i],a["fo_mo"][i],a["fo_mo_fore_mean"][i],
                  a["fw_mo"][i],a["fw_mo_fore_mean"][i])
                  
            cursor.execute(query,output)
    conn.commit()
    conn.close() 

def write_dataframe(conn,results,write_schema,table_name):
    """
    Convert results to a dataframe and push them to the database
    """
    test = []
    for k in results.keys():
        n = len(results[k]["Welldates"])
        results[k]["API"] = [k]*n
        results[k]["norm_date"] = range(1,n+1)
        test.append(results[k])
         
    df = pd.DataFrame()
    for t in test:
        x = pd.DataFrame.from_dict(t)
        df = df.append(x)
     
    engine = create_engine("mysql+mysqldb://aamir:pythonbyaamir@navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com:3306/enriched_data")
     
    df.to_sql(table_name,engine,if_exists='replace', chunksize=1000)

    return

def save_json(filename,results):
    
    with open(filename+'.json', 'w') as outfile:
        json.dump(results, outfile)
    
    return

def load_json(filename):
    
    with open(filename+'.json', 'r') as f:
        data = json.load(f)
    return data
    
def get_WellData(cursor,wellID,schema,table_name):
     """
     Reads the data from the the MySql DB table.
     """
     #start=time.time()
     query="SELECT * \
             FROM "+schema+"."+table_name+" WHERE API_prod=%s ORDER BY report_date_mo ASC ;"
             
     cursor.execute(query,[wellID])
     row = cursor.fetchall()
         
     data_obj = {"Welldates":[], "proddays":[], "oil_pd":[], "oil_pd_fore_mean":[], "oil_pd_fore_std":[],"oil_pd_fore_p10":[],"oil_pd_fore_p90":[],
                 "wtr_pd":[],"wtr_pd_fore_mean":[],"wtr_pd_fore_std":[],"wtr_pd_fore_p10":[],"wtr_pd_fore_p90":[]}
    
     #Make single lists for each column they are bound by index
     for value in row:
        data_obj["Welldates"].append(value[1].strftime("%Y-%m-%d")) # extract operating dates for this well
        data_obj["proddays"].append(value[2])
        
        if value[3] == None:
            data_obj["oil_pd"].append(0)
        else:
            data_obj["oil_pd"].append(value[3])
        
        if value[4] == None:
            data_obj["oil_pd_fore_mean"].append(0)
        else:
            data_obj["oil_pd_fore_mean"].append(value[4])
          
        if value[5] == None:
            data_obj["oil_pd_fore_std"].append(0)
        else:
            data_obj["oil_pd_fore_std"].append(value[5])
            
        if value[6] == None:
            data_obj["oil_pd_fore_p10"].append(0)
        else:
            data_obj["oil_pd_fore_p10"].append(value[6])
            
        if value[7] == None:
            data_obj["oil_pd_fore_p90"].append(0)
        else:
            data_obj["oil_pd_fore_p90"].append(value[7])
              
        if value[8] == None:
            data_obj["wtr_pd"].append(0)
        else:
            data_obj["wtr_pd"].append(value[8])
        
        if value[9] == None:
            data_obj["wtr_pd_fore_mean"].append(0)
        else:
            data_obj["wtr_pd_fore_mean"].append(value[9])
          
        if value[10] == None:
            data_obj["wtr_pd_fore_std"].append(0)
        else:
            data_obj["wtr_pd_fore_std"].append(value[10])
            
        if value[11] == None:
            data_obj["wtr_pd_fore_p10"].append(0)
        else:
            data_obj["wtr_pd_fore_p10"].append(value[11])
            
        if value[12] == None:
            data_obj["wtr_pd_fore_p90"].append(0)
        else:
            data_obj["wtr_pd_fore_p90"].append(value[12])
            
     #print("Time Taken : "+str(round(((time.time()-start)),2))+" secs")
     #print          
     return data_obj
