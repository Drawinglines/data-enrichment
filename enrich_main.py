# -*- coding: utf-8 -*-
"""
Created on Fri Apr 29 17:38:27 2016
Data Enrichment
@author: Administrator
"""
from enrich_helpers import establish_DBconn,get_WellPids,get_WellData,create_table,write_DB,save_json,write_dataframe
import numpy as np
import time

import sql
import calendar
from datetime import datetime

schema = "results"
table_name = "FFR_NDK_best_copy"
write_schema = "enriched_data"
test_date = '2009-01-31'

def main():
    
    conn,cursor = establish_DBconn(write_schema)
    create_table(conn,cursor,write_schema,table_name)
    results = {}
    
    unique_ids = get_WellPids(cursor,schema,table_name)
    start=time.time()
    for wellID in unique_ids:
        #print "Reading API - ",wellID
        
        data = get_WellData(cursor,wellID,schema,table_name)
        
        """
        Calculate monthly values
        """
        data["oil_mo"] = calc_monthly(data["oil_pd"],data["proddays"])
        data["wtr_mo"] = calc_monthly(data["wtr_pd"],data["proddays"])
        
        """Using calendar days for forecasted values as it can be 0 proddays"""
        calendar_days = get_cal_days(data["Welldates"])
        data["oil_mo_fore_mean"] = calc_monthly(data["oil_pd_fore_mean"],calendar_days)
        data["wtr_mo_fore_mean"] = calc_monthly(data["wtr_pd_fore_mean"],calendar_days)
        
        
        """
        Calculate cumulative values
        """
        data["cum_oil_mo"],data["cum_oil_mo_fore_mean"] = calc_cum(data["Welldates"],data["oil_mo"],data["oil_mo_fore_mean"])
        data["cum_wtr_mo"],data["cum_wtr_mo_fore_mean"] = calc_cum(data["Welldates"],data["wtr_mo"],data["wtr_mo_fore_mean"])
        
        if data["cum_oil_mo"] == []:
            continue
        
        """
        Calculate wor
        """
        data["wor"] = calc_wor(data["wtr_pd"],data["oil_pd"])
        data["wor_fore_mean"] = calc_wor(data["wtr_pd_fore_mean"],data["oil_pd_fore_mean"])
        
        
        """
        Calculate gross
        """
        data["gross_mo"] = calc_gross(data["oil_mo"],data["wtr_mo"])
        data["gross_mo_fore_mean"] =  calc_gross(data["oil_mo_fore_mean"],data["wtr_mo_fore_mean"])
        

        """Calculate rate of flow of oil and rate of flow of water"""
        data["fo_mo"] = calc_flow_rate(data["oil_mo"],data["gross_mo"])
        data["fw_mo"] = calc_flow_rate(data["wtr_mo"],data["gross_mo"])
        
        data["fo_mo_fore_mean"] = calc_flow_rate(data["oil_mo_fore_mean"],data["gross_mo_fore_mean"])
        data["fw_mo_fore_mean"] = calc_flow_rate(data["wtr_mo_fore_mean"],data["gross_mo_fore_mean"])
        
        """Replace 0's with None before test date"""
        data = replace_zeros(data,test_date)
        
        results[wellID] = data
    
    print("Calculations done : "+str(round(((time.time()-start)),2))+" secs")
    #save_json(table_name,results)
    
    write_dataframe(conn,results,write_schema,table_name)
      
    #write_DB(conn,cursor,write_schema,table_name,results)
    print("Total Time Taken : "+str(round(((time.time()-start)),2))+" secs")


def get_cal_days(Welldates):
            
    #cd = [calendar.monthrange(x.year,x.month)[1] for x in [datetime.strptime(i,'%Y-%m-%d') for i in Welldates] ]
    cd = [x.day for x in [datetime.strptime(i,'%Y-%m-%d') for i in Welldates]]

    return cd

def replace_zeros(data,test_date):
    """
    Replace 0s with None so it shows Null in Db
    """    
    for k in data.keys():
        
        if "_fore_" in k and "cum_" not in k:
            i = data["Welldates"].index('2009-01-31')
            a,b = [None]*i,data[k][i:]
            a.extend(b)
            data[k] = a
    
    return data

def calc_flow_rate(val,gross):
    """
    Calculates the rate of flow of oil,water for original and forecast values.
    """
    return [round(float(a)/b,2) if b!=0 else 0 for a,b in zip(val,gross)]
    

def calc_gross(oil,water):
    """
    Calculate gross flow of water and oil.
    """
    return [a+b for a,b in zip(oil,water)]

        
def calc_wor(water,oil):
    """
    Calculate Wor.
    """
    return [round(float(a)/b,2) if b!=0 else 0 for a,b in zip(water,oil)]
    
def calc_cum(Welldates, og_val, fore_val):
    """
    Calculate cumulative values for original and forecasted values. 
    For cumulative of forecasted values consider original values before 2009-01-31.
    """
    try:
        i = Welldates.index('2009-01-31')
        a,b = og_val[:i],fore_val[i:]
        a.extend(b)
        
        c,d = np.cumsum(og_val),np.cumsum(a)
        
        return c.tolist(),d.tolist()
    except:
        return [],[]

def calc_monthly(val,proddays):
    """
    Calculate monthly production values by multiplying number of days with daily production values.
    """
    return [round(a*b,2) for a,b in zip(val,proddays)]
    
    
main()