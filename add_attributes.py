# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 12:14:22 2016
Create new attributes for data.
Comment out old functions that you dont want to compute.
@author: Administrator
"""
from __future__ import division
from time import strftime
import time
from multiprocessing import Pool
import datetime
import math
import numpy as np
#from decimal import *
import terraai_preprocessing.preprocessing.pp_main as pp_main
import terraai_preprocessing.preprocessing.pp_helpers as pp_helpers
import terraai_preprocessing.combinatorics.API_main_comb as combinator
from tqdm import tqdm



#Global parameters
READ_SCHEMA = "public_data"
DB_PRODTABLE_NAME = "bigprod_ts"
DB_MASTERTABLE_NAME = "big_master_detail"

WRITE_SCHEMA = "aamir"
WRITE_PROD_TABLE = DB_PRODTABLE_NAME+"_"+str(datetime.date.today())
WRITE_STATIC_TABLE = DB_MASTERTABLE_NAME+"_"+str(datetime.date.today())

MULTIPROCESSING = True

_TIMESERIES_FLAG = False
if "_ts" in DB_PRODTABLE_NAME:
    _TIMESERIES_FLAG = True

conn, cursor = pp_main.establish_db_conn()

def multiprocessor(inp):
    """Multiprocessing"""
    api_prod, structure, column_names, static_col_names, static_structure,\
    min_date = inp[0], inp[1], inp[2], inp[3], inp[4], inp[5]

    #Read production and static data
    if _TIMESERIES_FLAG:
        prod_data = pp_main.get_well_data_ts(cursor, structure, column_names,
                                             api_prod, READ_SCHEMA, DB_PRODTABLE_NAME)
    else:
        prod_data = pp_main.get_well_data(cursor, structure, column_names,
                                          api_prod, READ_SCHEMA, DB_PRODTABLE_NAME)


    prod_data, days_month = pp_helpers.impute_missing_data(prod_data)

    data_length = len(prod_data["report_date_mo"])

    #Get static data
    static_data = pp_main.get_static_data(cursor, static_structure, static_col_names,
                                          api_prod, READ_SCHEMA, DB_MASTERTABLE_NAME)

    #Calculate new parameters
#    prod_data, static_data = calc_zero_prod(prod_data, static_data, data_length)
#
#    prod_data, static_data = check_comp_hist(prod_data, static_data, data_length, min_date)
#
#    static_data = months_since_spud(prod_data, static_data)
#
#    prod_data = calc_downtime(prod_data, days_month, data_length)
#
#    prod_data, wor = wor_plus_1(prod_data)
#
#    prod_data = calc_gross(prod_data)
#
#    prod_data = calc_cum(prod_data)
#
#    prod_data = active_idle_flag(prod_data, static_data, data_length)
#
#    static_data = first_non_zero_prod(prod_data, static_data, data_length)
#
#    prod_data = alloc_fact_months(prod_data, wor, data_length)
#
#    static_data = calc_last_oil_mo(prod_data, static_data, data_length)

    static_data = calc_dt60_oil_pd(prod_data, static_data, str(strftime("%Y-%m-%d")), data_length)

    return prod_data, static_data



def main(state):
    """Handles all the function calls"""

    prod_final, static_final = [], []
    start_time = time.time()
    print "State -", state
    #conn, cursor = pp_main.establish_db_conn(database="terra")
    state_dates = read_first_date(cursor)

    #Get table formats
    column_names, structure = pp_main.describe_table(cursor, READ_SCHEMA, DB_PRODTABLE_NAME)
    static_col_names, static_structure = pp_main.describe_table(cursor,
                                                                READ_SCHEMA, DB_MASTERTABLE_NAME)

    #Get well APIs
    well_apis = pp_main.get_well_ids(cursor, column_names, state, READ_SCHEMA, DB_PRODTABLE_NAME)

    total_wells = len(well_apis)
    print "Total number of wells -", total_wells

    #Do the processing in batch
    step_size = 25000
    n_batch = int(np.ceil(total_wells/step_size))
    ind = [min(x*step_size, total_wells) for x in range(n_batch+1)]

    for temp_indx in xrange(n_batch):
        results, prod_final, static_final = [], [], []
        input_tuple = tuple((API, structure, column_names, static_col_names,
                             static_structure, state_dates[state]) for API in well_apis[ind[temp_indx]:ind[temp_indx+1]])
        print "Batch - {}/{}".format(temp_indx, n_batch-1)
        #Multiprocessing
        if MULTIPROCESSING:
            pool = Pool()
            tasks = pool.map_async(multiprocessor, input_tuple)

            tasks.wait()
            results = tasks.get()
            pool.close()
            pool.terminate()
            pool.join()
        else:
            #Sequential
            time.sleep(1)
            for inp in tqdm(input_tuple):

                data = multiprocessor(inp)
                results.append(data)

        for data in results:
            prod_final.append(data[0])
            static_final.append(data[1])

        print "\n Writing data to the database..."
        pp_main.write_DB(conn, cursor, static_final, True, WRITE_SCHEMA, WRITE_STATIC_TABLE, False)
        pp_main.write_DB(conn, cursor, prod_final, True, WRITE_SCHEMA, WRITE_PROD_TABLE, False)
        del results

    print "Total Time taken - "+ str(round((time.time()-start_time), 2))+" secs"

    print

def calc_dt60_oil(prod_data, static_data, supplied_date, data_length):
    """Last oil production from a well if the current production is 0 with a max window of 60 months"""

    value = 0.0
    supplied_date = datetime.strptime(supplied_date, '%Y-%m-%d').date()
    
    if prod_data["oil_mo"][-1] != 0:
       diff = pp_helpers.diff_month(prod_data["report_date_mo"][-1], supplied_date)
       
       if diff > 60 :
           pass
       else:
           indx = next((i for i, x in enumerate(prod_data["oil_mo"][::-1]) if x), None)
           value = (data_length-1) - indx

    static_data["dt60_oil"].append(prod_data["oil_mo"][value])
    
    return static_data

def calc_last_oil_mo(prod_data, static_data, data_length):
    """Calculate last_oil_mo and last_12_months_oil"""

    static_data["last_12_oil_mo"] = [np.sum(prod_data["oil_mo"][-12:])]
    static_data["last_oil_mo"] = [prod_data["oil_mo"][-1]]

    return static_data


def alloc_fact_months(prod_data, wor, data_length):
    """ Months where WOR allocation factor does not change """

    alloc_fact_months = [1]

    for indx_i in xrange(1, data_length):

        if abs(wor[indx_i-1] - wor[indx_i]) <= math.log(wor[indx_i-1] + 1):
            alloc_fact_months.append(alloc_fact_months[-1]+1)
        else:
            alloc_fact_months.append(1)

    temp = []
    while data_length != 0:
        for i in xrange(alloc_fact_months[data_length-1]):
                temp.append(alloc_fact_months[data_length-1])

        data_length -= alloc_fact_months[data_length-1]

    prod_data["alloc_fact_months"] = temp[::-1]

    return prod_data


def first_non_zero_prod(prod_data, static_data, data_length):
    """Find first non zero prod date for oil/gas/water"""

    col_names = ["oil_mo", 'wtr_mo', 'gas_mo']
    vals = []
    for key in col_names:
        for indx in xrange(data_length):
            if prod_data[key][indx] > 0:
                vals.append(indx)
                break
            continue

    if len(vals) != 0:
        date_indx = min(vals)
        static_data["first_prod_date"].append(prod_data['report_date_mo'][date_indx])
    else:
        static_data["first_prod_date"].append(None)

    return static_data


def active_idle_flag(prod_data, static_data, data_length):
    """ Check if well is active or idle """

    spud_date = static_data["spud_date"][0]
    if spud_date is None:
        #inferred spud date
        spud_date = prod_data["report_date_mo"][0]

    for indx in xrange(data_length):

        if prod_data["oil_mo"][indx] > 0 or prod_data["wtr_mo"][indx] > 0\
            or prod_data["gas_mo"][indx] > 0: #or inj_mo>0
            prod_data["active_flag_int"].append(1)
            prod_data["idle_flag_int"].append(0)
        else:
            prod_data["active_flag_int"].append(0)
            prod_data["idle_flag_int"].append(1)

        prod_data["active_idle_check_flag_int"].append(prod_data["active_flag_int"][indx]+\
                                                  prod_data["idle_flag_int"][indx])

        if prod_data["active_idle_check_flag_int"][indx] > 1:
            print "Error active_idle_flag : ", prod_data["API_prod"][0]

    return prod_data

def calc_cum(prod_data):
    """Calculate cumulatives"""
    col_names = ["oil_mo", "wtr_mo", "gas_mo"]
    for key in col_names:
        prod_data["cum_"+key] = np.cumsum(prod_data[key])

    temp = np.cumsum([(a/b) if b != 0 else 1.0 for a, b in zip(prod_data["cum_wtr_mo"], prod_data["cum_oil_mo"])])
    prod_data["cum_wor_plus_one"] = [val+1 for val in temp]
                                     
    return prod_data


def calc_gross(prod_data):
    "Calculate gross_mo"

    prod_data["gross_mo"] = [a+b for a, b in zip(prod_data["oil_mo"], prod_data["wtr_mo"])]
    return prod_data


def calc_zero_prod(prod_data, static_data, length):
    """total number of months with zero production and % of
    months with no production (total months zero prod / number of months prod)"""

    #unique, counts = np.unique(prod_data["oil_mo"], return_counts=True)
    #count_dict = dict(zip(unique, counts))
    nos_months = prod_data["oil_mo"].tolist().count(0.0)
    perc_months = round(((float(nos_months)/length) * 100), 2)

    static_data["zero_prod_months"] = [nos_months]#*length
    static_data["zero_prod_perc_months"] = [perc_months]#*length

    return prod_data, static_data


def wor_plus_1(data):
    """Calculate wor from combinatorics and add 1 to the values"""

    if len(data['wtr_mo']) != len(data['oil_mo']):
        print data['API_prod'][0]

    result = combinator.encoder(data['API_prod'], data['report_date_mo'], data['prod_days'],
                                data['oil_mo'], data['wtr_mo'], ['wor', 'time'],
                                ['linear', 'linear'], impute_data=False)

    wor = result["arg1"]

    data["wor_plus_one"] = [val+1 for val in wor]

    return data, wor



def calc_downtime(prod_data, days_month, data_length):
    """Calculate downtime of wells, and set flag if prod_days
    is more than number of days in month"""

    prod_data["down_time"], month_days = pp_helpers.down_time(prod_data["prod_days"],
                                                              days_month=days_month,
                                                              report_date_mo=[])

    for indx in xrange(data_length):

        if prod_data["prod_days"][indx] > month_days[indx]:
            prod_data["prod_days_chk_flag"].append(True)
            prod_data["prod_days"][indx] = float(month_days[indx])
            prod_data["down_time"][indx] = 0.0
        else:
            prod_data["prod_days_chk_flag"].append(False)

    return prod_data


def months_since_spud(prod_data, static_data):
    """Find months since spud date and set flag to true if the difference between
    spud date and first production date is more than 3 months"""

    spud_date = static_data["spud_date"][0]

    if spud_date is None:
        static_data["months_since_spud"] = [0] #* data_length
        static_data["spud_diff_minprod_flag"] = [False] #* data_length
    else:
        diff = pp_helpers.diff_month(spud_date, prod_data["report_date_mo"][0])
        static_data["months_since_spud"] = [diff] #* data_length

        if diff > 3:
            static_data["spud_diff_minprod_flag"] = [True] #* data_length
        else:
            static_data["spud_diff_minprod_flag"] = [False] #* data_length

    return static_data


def check_comp_hist(prod_data, static_data, data_length, min_date):
    """Check if well has all the production records"""
    #print prod_data["API_prod"][0]
    if prod_data["report_date_mo"][0] >= min_date:
        static_data["comp_hist_flag"] = [True] #* data_length
    else:
        static_data["comp_hist_flag"] = [False] #* data_length

    return prod_data, static_data


def read_first_date(cursor):
    """Get first date tables"""

    query = "SELECT \"state_API\", first_prod_date from state_joins.state_first_prod;"
    cursor.execute(query)
    rows = cursor.fetchall()

    state_dates = {}
    for row in rows:
        if row[0] is not None:
            state_dates[row[0]] = row[1]

    return state_dates


if __name__ == '__main__':
    #4, 30, 5 ,33, 16, 43, 40, 50, 25, 3,49
    for state in [3, 40, 5, 33, 16, 43, 50, 25, 3, 49, 4, 3]:
        main(state)
